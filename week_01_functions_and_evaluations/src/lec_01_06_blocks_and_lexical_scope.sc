package lec_01_06_blocks_and_lexical_scope

object lec_01_06_blocks_and_lexical_scope {
  
  def sqrt(x: Double): Double = {
    def abs(a: Double) =
      if (a >= 0) a else -a
      
    def isGoodEnough(guess: Double) =
      abs(guess * guess - x) / x < 0.0001
      
    def improvedGuess(guess: Double) =
      (guess + x / guess) / 2
      
    def sqrtIter(guess: Double): Double =
      if (isGoodEnough(guess)) guess
      else sqrtIter(improvedGuess(guess))
      
    sqrtIter(1)
  }                                               //> sqrt: (x: Double)Double
  
  sqrt(2)                                         //> res0: Double = 1.4142156862745097
  sqrt(144)                                       //> res1: Double = 12.000545730742438
  sqrt(1e-60)                                     //> res2: Double = 1.0000000031080745E-30
  sqrt(1e60)                                      //> res3: Double = 1.0000000031080746E30
  
  val x = 1                                       //> x  : Int = 1
  // **** blocks can be used anywhere we can use an expression
  val result = {
    val x = if ( { false && true
    true} )4 else 3
    // **** The last element of a block is an expression that defines its value
    x * x
    // **** The following line demonstrates interaction of blocks with visibility
  } + x                                           //> result  : Int = 17
}
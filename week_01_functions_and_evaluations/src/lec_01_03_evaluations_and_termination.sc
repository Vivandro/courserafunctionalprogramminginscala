package lec_01_03_evaluations_and_termination

object lec_01_03_evaluations_and_termination {
  def loop: Int = loop                            //> loop: => Int
  
  // **** a is passd by value (CallBV) while b is passed by name (CBN)
  // As such the argument passed for a is evaluated and the result replaces
  // all further occurrences of a. For b, on the other hand, we replace it using the
  // entire expression that was passed.  CBV is more efficient. CBN arguments
  // allow the program to terminate more often than CBV arguments.
  def foo(a: Int, b: => Int) = a * a              //> foo: (a: Int, b: => Int)Int
  
  foo(10, loop)                                   //> res0: Int = 100
}
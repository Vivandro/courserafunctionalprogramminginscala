package lec_01_05_examples_newtons_square_root

object lec_01_05_examples_newtons_square_root {
  // Netwon's square root method to compute sqrt(x)
  // Start with an initial estimate y (can pick y = 1)
  // Repeatedly improve the estimate by taking mean of y and x/y
  
  def abs(a: Double) = if (a >= 0) a else -a      //> abs: (a: Double)Double
  
  def isGoodEnough(guess: Double, x: Double): Boolean =
    abs(guess * guess - x)/x < 0.001              //> isGoodEnough: (guess: Double, x: Double)Boolean
  
  def improvedGuess(guess: Double, x: Double): Double =
    (guess + x / guess) / 2                       //> improvedGuess: (guess: Double, x: Double)Double
  
  def sqrtIter(guess: Double, x: Double): Double =
    if (isGoodEnough(guess, x)) guess
    else  sqrtIter(improvedGuess(guess, x), x)    //> sqrtIter: (guess: Double, x: Double)Double
  
  def sqrt(x: Double): Double =
    sqrtIter(1, x)                                //> sqrt: (x: Double)Double
    
  sqrt(2)                                         //> res0: Double = 1.4142156862745097
  sqrt(144)                                       //> res1: Double = 12.000545730742438
  
  sqrt(1e-6)                                      //> res2: Double = 0.0010000001533016628
  sqrt(1e60)                                      //> res3: Double = 1.0000788456669446E30
  
}
package lec_01_04_conditionals_and_value_definitions

object lec_01_04_conditionals_and_value_definitions {
  def abs(x: Int) = if (x >= 0) x else -x         //> abs: (x: Int)Int
  
  def loop: Int = loop                            //> loop: => Int
  
  def x = loop                                    //> x: => Int
  
  // **** def vs val is similar to CBN vs CBV
  //val y = loop
  
  def a = abs(-9)                                 //> a: => Int
  val b = abs(-9)                                 //> b  : Int = 9
  
}
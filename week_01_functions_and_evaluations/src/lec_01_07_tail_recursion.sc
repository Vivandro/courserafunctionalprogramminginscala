package lec_01_07_tail_recursion

object lec_01_07_tail_recursion {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  // Euclid's GCD algorithm
  def gcd(a: Int, b: Int): Int =
    if(b == 0) a else gcd(b, a % b)               //> gcd: (a: Int, b: Int)Int
    
  gcd(50, 100)                                    //> res0: Int = 50
  
  gcd(13, 71)                                     //> res1: Int = 1
  
  // in-efficient factorial
  def fact(n: Int): Int =
    if (n <= 1) n else n * fact(n - 1)            //> fact: (n: Int)Int
    
  fact(10)                                        //> res2: Int = 3628800
  fact(3)                                         //> res3: Int = 6
  
  // **** tail recursion. Sadly he did not introduce tailrec keyword yet.
  // Clarity trumps efficieny. Premature optimization is the root of all evil.
  // tail recursive factorial
  def factorial(n: Int): Int = {
    def factorialAcc(n: Int, acc: Int): Int =
      if(n <= 1) acc
      else factorialAcc(n - 1, n * acc)
    
    factorialAcc(n, 1)
  }                                               //> factorial: (n: Int)Int
  
  factorial(10)                                   //> res4: Int = 3628800
  factorial(3)                                    //> res5: Int = 6
}
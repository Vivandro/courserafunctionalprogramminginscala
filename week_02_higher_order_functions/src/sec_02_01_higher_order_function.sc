package sec_02_01_higher_order_function

object sec_02_01_higher_order_function {
  def sumOfInts(a: Int, b: Int): Int = if(a > b) 0 else a+sumOfInts(a+1, b)
                                                  //> sumOfInts: (a: Int, b: Int)Int
  sumOfInts(9, 9)                                 //> res0: Int = 9
  sumOfInts(9, 10)                                //> res1: Int = 19

  def id(x: Int) = x                              //> id: (x: Int)Int
  def squares(x: Int) = x * x                     //> squares: (x: Int)Int
  def factorial(x:Int): Int = if(x == 0) 1 else x*factorial(x-1)
                                                  //> factorial: (x: Int)Int
  // higher order function
  def sumOf(f:(Int => Int), a: Int, b: Int): Int =
    if(a > b) 0 else f(a) + sumOf(f, a+1, b)      //> sumOf: (f: Int => Int, a: Int, b: Int)Int

  sumOf(squares, 1, 3)                            //> res2: Int = 14

  // anonymous functions
  // this is syntactic sugar since we could as well have defined a function and passed it here
  sumOf(x => x*x, 1, 3)                           //> res3: Int = 14

  // tail recursive sersion of sumOf
  def sumOf_tailrec(f: Int => Int, a: Int, b: Int) = {
  // notice that we only passed what is necessary. rest was used by capturing context
  def sumAccumulator(a: Int, acc: Int): Int =
    if(a > b) acc else sumAccumulator(a+1, acc+f(a))

    sumAccumulator(a, 0)
  }                                               //> sumOf_tailrec: (f: Int => Int, a: Int, b: Int)Int

  sumOf_tailrec(id, 9, 10)                        //> res4: Int = 19
}
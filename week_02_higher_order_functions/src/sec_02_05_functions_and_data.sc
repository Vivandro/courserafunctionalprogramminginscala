package sec_02_05_functions_and_data

object sec_02_05_functions_and_data {
  val rat = new Rational(22, 7)                   //> rat  : sec_02_05_functions_and_data.Rational = (22.0 / 7.0)
  val st = new Rational(2, 5)                     //> st  : sec_02_05_functions_and_data.Rational = (2.0 / 5.0)
  rat.neg                                         //> res0: sec_02_05_functions_and_data.Rational = (-22.0 / 7.0)
  rat.sub(rat)                                    //> res1: sec_02_05_functions_and_data.Rational = (0.0 / 49.0)
}

// Just defining the following is enough to create a class:
// class Rational(numer: Double, denom: Double)

// To allow rat.denom, it is necessary to specify cal before the parameter names.
// TODO: Need to figure out what it means to specify the params with and without val.
//       Is it merely a question of private versus public? If so, this cryptic choice
//       is an unexpected wrong move from Scala designers!
//

class Rational(val numer: Double, val denom: Double) {
  def add(that: Rational) =
    new Rational(numer * that.denom + that.numer * denom,
                 denom * that.denom)
                 
  def neg() = new Rational(-numer, denom)
  
  def sub(that: Rational) = add(that.neg) // Reuse add to abide by DRY: Don't Repeat Yourself
  
  override def toString(): String = "(" + numer + " / " + denom + ")"
}
package sec_02_06_more_fun_with_rationals // Adding package at top since we are reusing names from another worksheet

object sec_02_06_more_fun_with_rationals {
  val rat = new Rational(22, 7)                   //> rat  : sec_02_06_more_fun_with_rationals.Rational = (22 / 7)
  val st = new Rational(2, 5)                     //> st  : sec_02_06_more_fun_with_rationals.Rational = (2 / 5)
  rat.neg                                         //> res0: sec_02_06_more_fun_with_rationals.Rational = (22 / -7)
  rat.sub(rat)                                    //> res1: sec_02_06_more_fun_with_rationals.Rational = (0 / 1)
  rat.denom                                       //> res2: Int = 7
  
  st lessThan rat                                 //> res3: Boolean = true
  rat lessThan st                                 //> res4: Boolean = false
  
  // val inf = new Rational(9, 0)
  // inf.add(inf)
  
  val two = new Rational(2)                       //> two  : sec_02_06_more_fun_with_rationals.Rational = (2 / 1)
}

// Copying the class from last lecture
class Rational(x: Int, y: Int) {
  require(y != 0, "denom must be nonzero")
  // ****
  // require() method enforces a pre-condition on the caller of the function from the creator
  // ****
  // assert() is used to check the code of the function itself
  
  // ****
  def this(x: Int) = this(x, 1)

  private def gcd(a: Int, b: Int): Int =
    if(b == 0) a else gcd(b, a % b)

  val numer: Int = x / gcd(x, y)
  val denom: Int = y / gcd(x, y)

  def add(that: Rational) =
    new Rational(numer * that.denom + that.numer * denom,
                 denom * that.denom)
                 
  def neg() = new Rational(-numer, denom)
  
  def sub(that: Rational) = add(that.neg) // Reuse add to abide by DRY: Don't Repeat Yourself
  
  def lessThan(that: Rational) = numer * that.denom < that.numer * denom
  
  def max(that: Rational) =
    if (this.lessThan(that)) that else this
  
  override def toString(): String = "(" + numer + " / " + denom + ")"
}
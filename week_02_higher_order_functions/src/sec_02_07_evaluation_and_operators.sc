package sec_02_07_evaluation_and_operators

object sec_02_07_evaluation_and_operators {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val a = new Rational(1, 2)                      //> a  : sec_02_07_evaluation_and_operators.Rational = (1 / 2)
  val b = new Rational(2, 3)                      //> b  : sec_02_07_evaluation_and_operators.Rational = (2 / 3)
  
  a < b                                           //> res0: Boolean = true
  a + b                                           //> res1: sec_02_07_evaluation_and_operators.Rational = (7 / 6)
  -a                                              //> res2: sec_02_07_evaluation_and_operators.Rational = (1 / -2)
  a - b                                           //> res3: sec_02_07_evaluation_and_operators.Rational = (1 / -6)
}

class Rational(x: Int, y: Int) {

  private def gcd(a: Int, b: Int): Int = if (0 == b) a else gcd(b, a % b)
  
  val numer = x / gcd(x, y);
  val denom = y / gcd(x, y);
  
  def <(that: Rational) = (numer * that.denom < that.numer * denom)
  def +(that: Rational) = new Rational(numer * that.denom + that.numer * denom, denom * that.denom)
  // **** unary prefix operator
  def unary_-() = new Rational(-numer, denom)
  // In the video he was using an unnecessarily cryptic syntax for the unary -. I will show that syntax for unary +
  def unary_+ : Rational = new Rational(numer, denom)
  // What we have above is useless obfuscation. What would have been useful is.
  def unary_~ = new Rational(numer, denom)
  // His example does make a point though. Th point is that, unary_-: Rational is a syntax error, but unary_- : Rational isn't.
  // That's because it is valid to have a unary -: operator too. So the only way to make your intention clear
  // to the compiler is by adding in a space to separate the symbols out and halt it's operator recognition engine.
  def -(that: Rational) = this + (-that)

  /* **** Precedence rules(in increasing order of precedence priority, determined by its first char):
  (all letters)
  |
  ^
  &
  < >
  = !
  :
  + -
  * / %
  (all other special chars)
   */

  override def toString() = "(" + numer + " / " + denom + ")"
}
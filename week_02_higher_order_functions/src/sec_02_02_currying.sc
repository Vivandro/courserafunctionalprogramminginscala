package sec_02_02_currying

object sec_02_02_currying {
  // define a function that returns a function
  def sum(f: Int => Int): (Int, Int) => Int = {
    def sumF(a: Int, b: Int): Int =
    if (a > b) 0 else f(a) + sumF(a + 1, b)
    sumF
  }                                               //> sum: (f: Int => Int)(Int, Int) => Int
  
  def square(x: Int) = x * x                      //> square: (x: Int)Int
  // consecutive step wise application
  (sum(square))(1, 3)                             //> res0: Int = 14
  sum(square)(1, 3)                               //> res1: Int = 14
  
  // Currying: special syntax due to utility of this type of functions
  // def product(f: Int => Int): (Int, Int) => Int
  // versus
  // def product(f: Int => Int)(a: Int, b: Int): Int
  // there are 3 differences in the signature. why would anyone come up with such subtle differences! why! why! why!
  // gotta answer this some day
  def product(f: Int => Int)(a: Int, b: Int): Int =
  // 4th difference makes this neat - you return an expression instead of the function!
  // the expression calls the "inner function" by calling the outer function, in a manner
  // exactly similar to how the outer caller will!
    if (a > b) 1 else f(a) * product(f)(a + 1, b) //> product: (f: Int => Int)(a: Int, b: Int)Int
                                                  
                          
  (product(square))(0, 10)                        //> res2: Int = 0
  product(square)(3, 4)                           //> res3: Int = 144
      
  // BUT: I don't see how this form can be made tail recrusive.
  // Let's try it
  // gotta keep the same signature
  def productTailrec(f: Int => Int)(a: Int, b: Int): Int = {
    def pAccumulator(a: Int, acc: Int): Int =
      if(a > b) acc else pAccumulator(a + 1, f(a) * acc)
      
    pAccumulator(a, 1)
  }                                               //> productTailrec: (f: Int => Int)(a: Int, b: Int)Int
  
  (productTailrec(square))(0, 10)                 //> res4: Int = 0
  productTailrec(square)(3, 4)                    //> res5: Int = 144
  
  // BUT: I think we may have to supply the ( a, b ) params immediately. so it
  // should not be possible to save product(square)
  val sumOfSquares = sum(square)                  //> sumOfSquares  : (Int, Int) => Int = <function2>
  //val productOfSquares = product(square) // ERROR! just as expected
  // Based on the compiler error message, replacing the arguments with _ might work
  val productOfSquares = product(square)(_, _)  // Yes, it does help us create "partially applied functions"
                                                  //> productOfSquares  : (Int, Int) => Int = <function2>
                                                  
  productOfSquares(3, 4)                          //> res6: Int = 144
  (productOfSquares)(0, 10)                       //> res7: Int = 0
  
  // Exercises:
  // 1. write a product function
  // 2. write a factorial function in terms of the product function
  // 3. generalize sum and product functions to get a single function that can be made to do either
  
  // Exercise 1 : Done already!
  
  // Exercise 2 : factorial function in terms of the product function
  def factorial(n: Int) = if(n <= 1) 1 else product(x => x)(1, n)
                                                  //> factorial: (n: Int)Int
                                                  
  // Oops, this test failed right away. so I updated the code. good that i thought of this useful case
  factorial(0)                                    //> res8: Int = 1
  factorial(1)                                    //> res9: Int = 1
  factorial(2)                                    //> res10: Int = 2
  factorial(3)                                    //> res11: Int = 6
  factorial(10)                                   //> res12: Int = 3628800
  
  // Exercise 3 : generalize sum and product functions to get a single function that can be made to do either
  // I have seen these lectures a few weeks ago, and I have used C++ STL sort with predicate to know how
  // can be solved. so, here goes my attempt.
  // how do i want to call this method?
  // accumulate(products)(updateA, boundaryCheckAB, boundaryValue)(a, b)
  // obviously, i will have to go through multiple passes over the code. here they go
  def accumulate1(accumulationLogic: (Int, Int) => Int)(a: Int, b: Int): Int =
    accumulationLogic(a, b)                       //> accumulate1: (accumulationLogic: (Int, Int) => Int)(a: Int, b: Int)Int
    
  accumulate1((x, y)=>x+y)(2, 3)                  //> res13: Int = 5
 
  type myInt = Int // just an example to test what is possible
  type Acctype = (Int, Int) => Int
  def accumulate2(accExpr: Acctype)(a: Int, b: Int): Int = accExpr(a, b)
                                                  //> accumulate2: (accExpr: (Int, Int) => Int)(a: Int, b: Int)Int
                                                  
                                                  
  // type Acctype = (Int, Int) => Int // defined earlier
  type Nexttype = Int => Int
  type Predicatetype = (Int, Int) => Boolean
  
  def accumulate3(accExpr: Acctype)(getNext: Nexttype, doneYet: Predicatetype, endValue: Int)(a: Int, b: Int): Int =
    if(doneYet(a, b)) endValue else accExpr(a, accumulate3(accExpr)(getNext, doneYet, endValue)(getNext(a), b))
                                                  //> accumulate3: (accExpr: (Int, Int) => Int)(getNext: Int => Int, doneYet: (In
                                                  //| t, Int) => Boolean, endValue: Int)(a: Int, b: Int)Int
                                                  
                                                  
  
  accumulate3((x, y)=>x+y)((x=>x+1), ((x, y) => x>y), 0)(1, 4)
                                                  //> res14: Int = 10
                                                  
  accumulate3((x, y)=>x*y)((x=>x+1), ((x, y) => x>y), 1)(1, 4)
                                                  //> res15: Int = 24
                                                  
  
  def accumulate(accExpr: Acctype)(getNext: Nexttype, doneYet: Predicatetype, endValue: Int)(a: Int, b: Int): Int = {
    def accumulateTailrec(a: Int, b: Int, acc: Int): Int =
      if (doneYet(a, b)) acc else accumulateTailrec(getNext(a), b, accExpr(a, acc))
      
    accumulateTailrec(a, b, endValue)
    // We have to initialize the accumulator to the endValue because the result depends on this.
    // in that sense, the end value is also the init value.
    // also, doing this means we write this: if (doneYet(a, b)) acc
    // instead of writing this: if (doneYet(a, b)) accExpr(a, acc)
  }                                               //> accumulate: (accExpr: (Int, Int) => Int)(getNext: Int => Int, doneYet: (Int
                                                  //| , Int) => Boolean, endValue: Int)(a: Int, b: Int)Int
                                                  

  accumulate((x, y)=>x+y)((x=>x+1), ((x, y) => x>y), 0)(1, 4)
                                                  //> res16: Int = 10
                                                  
  accumulate((x, y)=>x*y)((x=>x+1), ((x, y) => x>y), 1)(1, 4)
                                                  //> res17: Int = 24
                                                  
                                                  
  val sums = accumulate((x, y)=>x+y)((x=>x+1), ((x, y) => x>y), 0)(_, _)
                                                  //> sums  : (Int, Int) => Int = <function2>
                                                  
  sums(1, 4)                                      //> res18: Int = 10
  sums(9, 10)                                     //> res19: Int = 19

  def addExpr(a: Int, b: Int) = a + b             //> addExpr: (a: Int, b: Int)Int
  def doneWhen(a: Int, b: Int) = a > b            //> doneWhen: (a: Int, b: Int)Boolean
  def nextN(n: Int) = n + 1                       //> nextN: (n: Int)Int
  val initialSum = 0                              //> initialSum  : Int = 0
  val summation = accumulate(addExpr)(nextN, doneWhen, initialSum)(_, _)
                                                  //> summation  : (Int, Int) => Int = <function2>
                                                  
  
  summation(1, 4)                                 //> res20: Int = 10
  summation(9, 10)                                //> res21: Int = 19
  
  def productExpr(a: Int, b: Int) = a * b         //> productExpr: (a: Int, b: Int)Int
  def initialProduct = 1                          //> initialProduct: => Int
  val productsInRange = accumulate(productExpr)(nextN, doneWhen, initialProduct)(_, _)
                                                  //> productsInRange  : (Int, Int) => Int = <function2>
                                                  
  productsInRange(1, 4)                           //> res22: Int = 24
  productsInRange(9, 10)                          //> res23: Int = 90
  
  def fact(n: Int): Int = productsInRange(1, n)   //> fact: (n: Int)Int
  fact(4)                                         //> res24: Int = 24
  fact(10)                                        //> res25: Int = 3628800
  fact(0)                                         //> res26: Int = 1
  
  // OOPS! in all this i've missed on providing the transformation function.
  // accumulate(sum)(squares)(nextN, doneYet, endValue)(a, b)
  // quite simple a requirement to accomodate
  
  type Transformertype = (Int => Int)
  def faccumulate(accExpr: Acctype)(transformer: Transformertype)(getNext: Nexttype, doneYet: Predicatetype, endValue: Int)(a: Int, b: Int): Int = {
    def accumulateTailrec(a: Int, b: Int, acc: Int): Int =
      if (doneYet(a, b)) acc else accumulateTailrec(getNext(a), b, accExpr(transformer(a), acc))
      
    accumulateTailrec(a, b, endValue)
  }                                               //> faccumulate: (accExpr: (Int, Int) => Int)(transformer: Int => Int)(getNext:
                                                  //|  Int => Int, doneYet: (Int, Int) => Boolean, endValue: Int)(a: Int, b: Int)
                                                  //| Int
                                                  
                                                  
  faccumulate(addExpr)(square)(nextN, doneWhen, initialSum)(1, 4)
                                                  //> res27: Int = 30
                                                  
  
  // On to what Martin has to say...
  // Martin uses some really useful terminology
  // map function, unit value, combining function,...we are after a version of map reduce
  // of course this is simpler because it does not parametrize doneYet and nextN functions
  def mapReduce(map: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int, b: Int): Int =
    if(a > b) unit else combine(map(a), mapReduce(map, combine, unit)(a+1, b))
                                                  //> mapReduce: (map: Int => Int, combine: (Int, Int) => Int, unit: Int)(a: Int,
                                                  //|  b: Int)Int
                                                  
                                                  
                                                  
  mapReduce(square, addExpr, initialSum)(1, 4)    //> res28: Int = 30
                                                  
  
  // so, the sum function defined using mapReduce looks like this:-
  def sum_mr(f: Int => Int)(a: Int, b: Int) = mapReduce(f, addExpr, initialSum)(a, b)
                                                  //> sum_mr: (f: Int => Int)(a: Int, b: Int)Int
                                                  
                                                  
  sum_mr(square)(1, 4)                            //> res29: Int = 30
  
  // and, you could also do the same with my version
  def sum_facc(f: Int => Int)(a: Int, b: Int) = faccumulate(addExpr)(f)(nextN, doneWhen, initialSum)(a, b)
                                                  //> sum_facc: (f: Int => Int)(a: Int, b: Int)Int
                                                  
  sum_facc(square)(1, 4)                          //> res30: Int = 30
                                                  
  // So, in the end, what matters most, is providing a simple interface to the end user
  // and an efficient and flexible implementation internally
  
  // I need to develop a good style. The code is starting to look write-only code.
}
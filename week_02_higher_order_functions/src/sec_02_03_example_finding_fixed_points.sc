package sec_02_03_example_finding_fixed_points

import math._

object sec_02_03_example_finding_fixed_points {

  // for some functions f(x), by repeated application of f() to f(x), you reach a stage
  // where you approach a fixed point.
  // almost copying this method from the video
  val tolerance = 0.0001;                         //> tolerance  : Double = 1.0E-4
  def isCloseEnough(x: Double, y: Double) =
    abs( (x - y) / x ) / x < tolerance            //> isCloseEnough: (x: Double, y: Double)Boolean

  type NextDoubleType = Double => Double
  def fixedPoint(f: NextDoubleType)(firstGuess: Double) = {
    def improveGuess(guess:Double): Double = {
      val nextGuess = f(guess)
      if (isCloseEnough(guess, nextGuess)) nextGuess
      else improveGuess(nextGuess)
    }
    improveGuess(firstGuess)
  }                                               //> fixedPoint: (f: Double => Double)(firstGuess: Double)Double
  
  // f(x) = (1+x/2) is one such function.
  fixedPoint(x => 1 + x / 2)(1)                   //> res0: Double = 1.999755859375
  
  // is square root such a function?
  // sqrt(x) = y, such that y * y = x
  // so, y = x / y
  // this means, we will find the square root of N by the repeated application
  // of the function f(y) = N / y
  
  // The following version of squareRoot does not converge though. For squareRoot(2)
  // the guess oscillates between 1 and 2 without every converging to a single value.
  //// def squareRoot(N: Double) = fixedPoint(y => N / y)(1)
  //// squareRoot(2)
  // One way to control such oscillations is to prevent the estimation from varying too much. This is
  // done by averaging successive values of the original sequence.
  
  def squareRoot(N: Double) = fixedPoint(y => (y + N / y) / 2)(1)
                                                  //> squareRoot: (N: Double)Double
  squareRoot(2)                                   //> res1: Double = 1.4142135623746899
  
  // so, the average damping functions can be abstracted out into its own reusable function
  def averageDamp(f: Double => Double)(x: Double): Double = (x + f(x)) / 2
                                                  //> averageDamp: (f: Double => Double)(x: Double)Double
  // Exercise: write a square root function using fixedPoint and averageDamp
  def squareRoot1(N: Double) = fixedPoint( averageDamp(y => N / y) )(1)
                                                  //> squareRoot1: (N: Double)Double
  
  squareRoot1(2)                                  //> res2: Double = 1.4142135623746899
}
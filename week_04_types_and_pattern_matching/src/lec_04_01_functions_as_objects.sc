package lec_04_01_functions_as_objects

object lec_04_01_functions_as_objects {
  // **** functions are objects. methods are not.
  
  val foo = {
    class Whatever extends Function1[Int, Int] {
      def apply(x: Int) = x * x
    }
    new Whatever
  }                                               //> foo  : Int => Int = <function1>
  // **** Since foo is the instance of a class that extends Function1, it inherits the apply(x:Int) method
  // Scala has Function1 to Function22 to allow for between 1 and 22 arguments
  foo.apply(3)                                    //> res0: Int = 9
  foo(3)                                          //> res1: Int = 9

  // **** Anonymous class syntax. The intermediate class related syntax is removed to make
  //  it seem as if we are directly instantiating a trait
  // **** Looks like, Function2 allows us to implement apply with a single param as well as 3 or more.
  // So, now I wonder what utility these classes serve. Couldn't a single Function trait suffice?
  val f = new Function2[Int, Int, Int] {
    def apply(x: Int) = x * x
    def apply(a: Int, b: Int) = a * b
    def apply(a: Int, b: Int, c: Int) = a * b * c
    def apply(a: Int, b: Int, c: Int, d: Int) = a * b * c * d
  }                                               //> f  : (Int, Int) => Int{def apply(x: Int): Int; def apply(a: Int,b: Int,c: I
                                                  //| nt): Int; def apply(a: Int,b: Int,c: Int,d: Int): Int} = <function2>
  f.apply(3)                                      //> res2: Int = 9
  f(3)                                            //> res3: Int = 9
  f(3, 4)                                         //> res4: Int = 12
  f(3, 4, 5)                                      //> res5: Int = 60
  f(1, 2, 3, 4)                                   //> res6: Int = 24
  
  trait VivFunc[T, V]
  val voo = new VivFunc[Int, Int] {
    def apply(x: Int) = x * x
    def apply(a: Int, b: Int) = a * b
    def apply(a: Int, b: Int, c: Int) = a * b * c
    def apply(a: Int, b: Int, c: Int, d: Int) = a * b * c * d
    def apply(a: Int, b: String): Int = a
  }                                               //> voo  : lec_04_01_functions_as_objects.lec_04_01_functions_as_objects.VivFun
                                                  //| c[Int,Int]{def apply(x: Int): Int; def apply(a: Int,b: Int): Int; def apply
                                                  //| (a: Int,b: Int,c: Int): Int; def apply(a: Int,b: Int,c: Int,d: Int): Int; d
                                                  //| ef apply(a: Int,b: String): Int} = lec_04_01_functions_as_objects.lec_04_01
                                                  //| _functions_as_objects$$anonfun$main$1$$anon$2@2ae9e72a
  
  /* **** Hmm, after some more thinking I have the following observations to make. The reasoning behind
     Eta Expansion is a TODO for me. Need to get to it later today.
     That being said, if we assume that we need Eta Expansion, then the Function1 to Function22 traits make total sense.
     For eta expansion, we want to be able to wrap any function inside of a class that has a single method that in turn
     calls the function that we want to wrap. So, f(x) will be wrapped into the instance of an anonumous class and then
     invoked by calling a hidden method on the anonymous instance of that anonymous class. Let's call the class AnonXXX and
     the instance anonYYY. Then f(x) results in the creation of an anonmous class we can call AnonXXX and it's single instance
     anonYYY. f(x) gets translated into anonYYY.apply(x), where apply is the hidden method.
    
     We can have a call to f(x) happening in multiple places. If they are within the same scope, potentially the compiler could
     re-use anonYYY. If they occurr in different scopes, compiler can reuse AnonXXX as long as it knows to use the same name.
     Instead, if AnonXXX extends a standard trait and the name AnonXXX is a function of the name of the well known trait combined with
     the FQDN of the function it is packaging, the compiler could easily re-use the trait template instance it created when it
     created AnonXXX.
     
     Function1 to Function22 serve the purpose of the well-known-traits required for this to work. They also provide the template to
     perform the translation between the method arguments and function parameters.
     
     So, if we instantiate these traits explicitly, we can only add apply methods that let us combine a subset of features
     that we will get by re-using each FunctionN separately. For example, Function1[Int, Int] will only ever allow us to
     add apply methods that take N Ints and give back one Int. Function2[Int, String, Int] will actually not be implemented
     by the apply(Int, Int): Int method we may have implemented in the instance of trait Function[Int, Int] unless we explicitly
     implement that method. Implementing all the combinations is tedious and that's what templates help us with. So
     we better just re-use the FunctionN traits in this case and get rid of all the unnecessary code that we might
     need to otherwise test and maintain.
        
  */
    
  
  new Nil                                         //> res7: lec_04_01_functions_as_objects.Nil[Nothing] = .
  List()                                          //> res8: lec_04_01_functions_as_objects.Nil[Nothing] = .
  List(2)                                         //> res9: lec_04_01_functions_as_objects.Cons[Int] =  2 .
  List(1, 3)                                      //> res10: lec_04_01_functions_as_objects.Cons[Int] =  1  3 .
}


trait List[T] {
  def isEmpty: Boolean
  def head: T
  def tail: List[T]
}

class Nil[T] extends List[T] {
  val isEmpty = true
  // **** I had defined head and tail as val rather than def and spent a crazy 15 minutes trying to figure
  // why I was getting Error(nil.head) while creating any object of my List class. Lessons learnt!:-
  // 1. Always re-write everything the way I did here. It helps test my understanding.
  // 2. Use def whenever we want to be lenient - don't throw the exceptions unless someone actually tries to access nil.head/tail
  def head = throw new Error("nil.head")
  def tail = throw new Error("nil.tail")

  override def toString = "."
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  val isEmpty = false
  override def toString = " " + head + " " + tail
}

// **** The apply(...) methods have to be invoked on an instance of a class. But we want to define them for usage like:
// List(1) List(), List(1, 45, 9). This means we cannot use new before List(...). Which means, that we need a singleton
// instance of List. We do this by creating the companion object for the List class.
object List {
  def apply[T]() = new Nil
  def apply[T](a: T) = new Cons(a, new Nil)
  def apply[T](a: T, b: T) = new Cons(a, new Cons(b, new Nil))
}
package lec_04_02_objects_everywhere

object lec_04_02_objects_everywhere {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val x0 = zero                                   //> x0  : lec_04_02_objects_everywhere.zero.type =  [0]
  val x1 = new Succ(x0)                           //> x1  : lec_04_02_objects_everywhere.Succ =  [1]
  val x2 = new Succ(x1)                           //> x2  : lec_04_02_objects_everywhere.Succ =  [2]
  // Makes sense for the following to be false
  x2 == new Succ(x1)                              //> res0: Boolean = false
  x0+x0                                           //> res1: lec_04_02_objects_everywhere.Nat =  [0]
  x0+x2                                           //> res2: lec_04_02_objects_everywhere.Nat =  [2]
  x2+x2                                           //> res3: lec_04_02_objects_everywhere.Nat =  [4]
  val x4 = x2+x2                                  //> x4  : lec_04_02_objects_everywhere.Nat =  [4]
  val x8 = x4+x4                                  //> x8  : lec_04_02_objects_everywhere.Nat =  [8]
  val x16 = x8+x8                                 //> x16  : lec_04_02_objects_everywhere.Nat =  [16]
  val x32 = x16+x16                               //> x32  : lec_04_02_objects_everywhere.Nat =  [32]
  val x64 = x32+x32                               //> x64  : lec_04_02_objects_everywhere.Nat =  [64]
  val x128 = x64+x64                              //> x128  : lec_04_02_objects_everywhere.Nat =  [128]
  
  // Even the following will be false because we created x1 by using new. Has be just tried getting x0's successor
  // both would have been equal.
  x1 == x0+x1                                     //> res4: Boolean = false
  // There you go
  val xx1 = x0.successor                          //> xx1  : lec_04_02_objects_everywhere.Nat =  [1]
  xx1 == x0+x1                                    //> res5: Boolean = true
  
  // And these should be true also
  x1 == (x1 + x0)                                 //> res6: Boolean = true
  (x0+x1) == (x0+x1)                              //> res7: Boolean = true
  x4 == (x2+x2)                                   //> res8: Boolean = true

  // Now testing subtraction
  x4-x2                                           //> res9: lec_04_02_objects_everywhere.Nat =  [2]
  x16-x2-x4-x0-x8                                 //> res10: lec_04_02_objects_everywhere.Nat =  [2]
  x128-x128                                       //> res11: lec_04_02_objects_everywhere.Nat =  [0]
  
  val x3 = x2+x1                                  //> x3  : lec_04_02_objects_everywhere.Nat =  [3]
  val x7 = x2+x2+x3                               //> x7  : lec_04_02_objects_everywhere.Nat =  [7]
  // These should be equal...and they are!
  (x3-x1) == (x7-x4-x1)                           //> res12: Boolean = true
}

// Exercise 1: Implement Boolean in order to see if we can implement a primitive type as a class
abstract class Boolean {
  // **** add type parametrization to the method rather than the class because we we don't have Booleans fo something. Booleans
  // are complete types in themselves - it's a primitive type.
  def ifThenElse[T](t: => T, e: => T): T // => is our old friend that marks the arguments pass-by-name, or equivalent to def versus val
  // ifThenElse returns t if this is true and e if this is false. How this is evaluated is a question for another time.
  
  // **** I had to use True/False instead of true/false because of type mismatch. It was also fun to
  // design True/False that would make sense in this context
  def &&(that: => Boolean): Boolean = ifThenElse(that, False)
  def ||(that: => Boolean) = ifThenElse(True, that)
  def unary_!() = ifThenElse(False, True)
  
  // **** This one did give me a bit of a pause because I was thinking of equality in terms of object references versus value.
  // But value makes more sense here. Now,I have to pause to make the truth table!!!
  // this, that, this == that
  // T, T, T
  // T, F, F
  // F, T, F
  // F, F, T
  // if this, then that
  // else (i.e., if ! this) then !that
  def ==(that: Boolean): Boolean = ifThenElse(that, !that)  // Interesting. The compiler was able to tell that this is recursive due to unary !
                                                            // I, on the other hand, haven't thought about it yet.
 
  // Let's get first to the truth table for the next method
  // this, that, this != that
  // T, T, F
  // T, F, T
  // F, T, T
  // F, F, F
  // if this, then !that
  // else (i.e., if ! this) then that
  def !=(that: Boolean): Boolean = this == !that // Martin's implementation was also reasonable ifThenElse(!that, that)
                                        // His is in line with defining everything in terms of ifThenElse, but
                                        // he has already re-used a method we added by using !(). So whay not use
                                        // another method ==(), that we have also added.
}

object True extends Boolean {
  // **** hah....Although I came up with these two concrete objects on my own(after prompting
  // from the compiler, I actually had to see Martin's implementation of the ifThenElse method in these objects
  // to realize how obvious this implementation should have been! I had it set to ??? for a very long time!
  def ifThenElse[T](t: => T, e: => T): T = t
}

object False extends Boolean {
  def ifThenElse[T](t: => T, e: => T): T = e
}

// Exercise 2: Provide abstract class Nat that represents non-negative numbers.
// If we can do that, then does it mean that we can represent all primitive types in terms of classes and objects?
// Martin gave the list of methods that need to be implemented.
abstract class Nat {
  def isZero: Boolean
  def predecessor: Nat
  //def successor = new Succ(this) // **** It is slightly troubling that we will be creating new instances every time. We should only ever create one successor.
  var succ: Nat = null  // var does go against FP, but it is way to inefficient to not use it in this case. Will stick with
                        // it unless Martin has an efficient alternative.
  def successor: Nat = {
    if (null == succ) succ = new Succ(this)
    succ
  }
  def +(that: Nat): Nat
  def -(that: Nat): Nat
  def index: Int  // Martin did not ask us to add this, but I'm adding this only to make toString easier to understand.
                  // It will not have any impact on the way Nat works apart from the toString method.
  override def toString = " [" + index + "]"
}

// **** **** **** Martin even told us that we need to implement a sub-object Zero (this was already obvious) and a sub class
// Succ(n: Nat). The requirement for a sub class was obvious because Nat was an abstract class and Zero was going to
// be an independent object. But it wasn'tapparent to me immediately that the subclass should be called Succ (for Successor)
// and such a definition would make it easy to think about natural numbers. Basically, each object is either a Zero, or a
// successor. So, only Zero is an object I can identify with existing math concepts. Succ is a recursive definition. So, it
// has no basis in math the way I understand it. 6 is a number in itself. I do not think of it as 5's successor or
// 7's pre-decessor. But Martin's Succ class makes us think in these recursive terms.

// At first attempt, I implemented 7 out of the required 10 methods correctly. Cannot wrap my head around the remaining
// 3 though!  (zero.+(), Succ.+(), Succ.-()). The only question surrounding those 3 methods is, how do we ask the recurive
// definition to stop? So, off I go to watch Martin's lecture. Wiat...predecessors will always hit a wall of zero.
// That should help us count. So, to add y to x (first change the method signatures to use y rather than that),
// we keep getting this's successor while y is not zero. then, to the successor, we add y's predecessor. Yay, that should
// leave only the Succ.-() method for Martin. On to the +() implementations.
// Wow! In the end this wasn't so tough after all! Ofcourse the key that helped was the abstract interface that Martin
// came up with. It logically lead to the correct implementation. It would also have helped if I had thought of the
// toString visualization earlier. But I solved everything except Succ.-() before I decided to visualize everything.
// Now gotta see what his implementation is and note any differences.
object zero extends Nat {
  val isZero = True
  val index = 0
  def predecessor = throw new Error("zero.predecessor")// haha, I used def right away in this case instead of val the way I had used in an earlier exercise
  
  // Ah...Martin has a better version of +():
  // def +(that: Nat) = that
  // Haha. Should have been obvious. I got carried away with my visualization. It will obviously break my
  // interpretation of natural numbers though. 0 + 7 gives me 7th successor of 0. And since pure 7 may not
  // be 0's 7th successor, equality check will fail. I can actually use Martins implementation without breaking
  // my check for equality if I can disallow the use of new to create Succ objects and force the user to only use
  // successor, predecessor, +() and -() for getting natural numbers. Actually that would make sense since real world
  // does not have new.
  def +(y: Nat) = if (y.isZero == True) zero else successor + y.predecessor
  
  def -(y: Nat) = if(y.isZero == True) this else throw new Error("Nat.negative")
}

class Succ(n: Nat) extends Nat {
  val isZero = False
  def index = predecessor.index + 1
  def predecessor = n

  def +(y: Nat) = if (y.isZero == True) this else successor + y.predecessor
  // Hah, this can't be that difficult either. I can just keep going back for both this as well as y until
  // y hits zero!
  def -(y: Nat) = if(y.isZero == True) this else predecessor - y.predecessor
}
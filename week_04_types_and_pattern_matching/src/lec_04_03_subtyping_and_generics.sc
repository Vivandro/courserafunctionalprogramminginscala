package lec_04_03_subtyping_and_generics

object lec_04_03_subtyping_and_generics {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  /*
     //**** Liskov Substitution Principle: (Paraphrased). If you can prove a property p(x)
            about an object x of class A, then you should be able to prove p(y) for an object
            y of class B which is a subclass os class A.
     LSP is used to decide whether or not we should allow co-variance for generic types.
     
     List[T] is a covariant type while Array[T] isn't. The only reason I could see is that Array
     is mutable while List is not. Being mutable, array allows you to do this:
     class IntSet{}
     object Empty extends IntSet {}
     class NonEmpty extends IntSet {}
     
     Array[NonEmpty] a = new NonEmpty(...)
     Array[IntSet] b = a; // allowed if Arrays were co-variant because we can always allow a superclass ref to refer to a subclass object
     b(0) = Empty; // runtime crash!
     
     To improve type-safety such a possibility should be avoided. LSP is seen to be broken here because:-
     NonEmpty and Empty are subclasses of IntSet. If we made Array covariant, then Array[NonEmpty] is a subtype of Array[IntSet].
     Array[IntSet] can be successfully assigned elements from Empty as well as NonEmpty.
     It's supposed subclass Array[NonEmpty] will give us a runtime exception if we try assigning an Empty object as its element.

     --------
     Now, how is it different from exceptions caused when assigning Empty object to a NonEmpty reference?
     is: IntSet = Empty
     ne: NonEmpty = Empty
     Trick question - this will give you a compile time error due to type mismatch, so no runtime exceptions here. But we are able
     to assign Empty to is but not ne. Does that not violate LSP? Of course not. is and ne are references and not actually objects.
     It would have been a violation if is.x = Empty was allowed but ne.x = Empty wasn't.
     
     --------
     Hmm...this begs the question (to use one of Martin's pet phrases), does overriding superclass methods some times result in
     subclasses that violate LSP?
     e.g. Cons.head gives us back a result. Nil.head throws an exception.
     Does LSP have a more restricted meaning than I figure it to have? I guess my understanding here is incorrect.
     // **** TODO: Try to get a clearer resolution of the issue I have mentioned above.
     
     --------
     What about :
     ne: NonEmpty = new NonEmpty(...)
     is: IntSet = ne
     is = Empty
     Well, what about it? it is all valid code. And it does not violate LSP at all.

     --------
     Since we cannot mutate contents of a List, everything provable on List[IntSet] is also provable on List[NonEmpty].
     
     // **** I went through Lec 4.4 as well, but rather than creating notes similar to the ones above, I'm recommending myself
             further viewings of the lecture. It is a useful exercise to solve exercises while pausing the video. It is
             insightful. The fun of it will be lost if I just write those insights down rather than discover them again
             via Martin's guided tour.
   */
}
package lec_03_03_polymorphism

/* **** Two types of polymorphism covered until now:
  - Subclassing: List can take the form of a Nil or Cons list.
  - Generics: Same code for List can be used to instantiate lists of different types.
 */

object lec_03_03_polymorphism {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  val l1 = new Cons(10, new Nil)                  //> l1  : lec_03_03_polymorphism.Cons[Int] = ( 10, . )
  val l2 = new Cons(20, new Nil)                  //> l2  : lec_03_03_polymorphism.Cons[Int] = ( 20, . )
  val l1l2 = new Cons(l1, new Cons(l2, new Nil) ) //> l1l2  : lec_03_03_polymorphism.Cons[lec_03_03_polymorphism.Cons[Int]] = ( ( 
                                                  //| 10, . ), ( ( 20, . ), . ) )
  val l12 = new Cons(10, new Cons(20, new Nil))   //> l12  : lec_03_03_polymorphism.Cons[Int] = ( 10, ( 20, . ) )
  
  // **** generic function
  def nth[T](l: List[T], n: Int): T =
    if (l.isEmpty) throw new IndexOutOfBoundsException
    else if (n == 0) l.head
    else nth(l.tail, n-1)                         //> nth: [T](l: lec_03_03_polymorphism.List[T], n: Int)T
    
  nth(l12, 0)                                     //> res0: Int = 10
  nth(l12, 1)                                     //> res1: Int = 20
  nth(l1l2, 1)                                    //> res2: lec_03_03_polymorphism.Cons[Int] = ( 20, . )
  // **** playing around with Nil typing
  // equivalent to Nil[Nothing]
  val lnil = new Nil                              //> lnil  : lec_03_03_polymorphism.Nil[Nothing] = .
  new Cons(null, new Nil)                         //> res3: lec_03_03_polymorphism.Cons[Null] = ( null, . )
  val lnothing = new Nil[Nothing]                 //> lnothing  : lec_03_03_polymorphism.Nil[Nothing] = .
  // false...obviously - it is not a singleton
  lnil == lnothing                                //> res4: Boolean = false
  // false...had to make sure. so, the default implementation of equals uses reference matching to conclude these are unequal
  lnil.equals(lnothing)                           //> res5: Boolean = false
}

// **** generic class
trait List[T] {
  def head: T
  def tail: List[T]
  def isEmpty: Boolean
}

// **** subclassing in generics
class Nil[T] extends List[T] {
  val isEmpty = true
  def head = throw new NoSuchElementException
  def tail = throw new NoSuchElementException
  override def toString = "."
}

class Cons[T](val head: T, val tail: List[T]) extends List[T] {
  val isEmpty = false
  override def toString = "( " + (if(null == head) "null" else head.toString()) + ", " + tail.toString() + " )"
}
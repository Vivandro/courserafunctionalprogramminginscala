package lec_03_02_class_organization

object lec_03_02_class_organization {
  // **** Traits can contain fields and concrete methods. But the cannot have value parameters.
  // Singer(tune: String) is not allowed, but ProSinger(tune: String) is allowed.
  trait Singer/*(tune: String) */ {
    val canSing: Boolean
    val okTune = "la la la la la"
    def outOfTune = "la ta tri liiiiii"
    def sing(): Unit = println(okTune)
  }
  abstract class ProSinger(tune: String) extends Singer
  // **** It does make some sense to not have value parameters for traits. If we allowed that,
  //      then the subclass constructor will have to allow for all these parameters. Would become
  //      tedious quickly.
  abstract class Boo(tune: String) extends ProSinger(tune: String)
  
  trait Painter {
    val canPaint: Boolean
  }
  
  trait Mathematician {
    val canAdd: Boolean
  }
  
  // **** Classes, objects and traits can inherit from at most one class, but arbitrarily many traits.
  trait Artist extends Singer with Painter
  
  // **** Subclass from one and have more than one traits
  // **** You can even subclass from a trait.
  class Joker extends Mathematician with Painter with Singer with Artist {
    val canAdd = true
    val canSing = true
    val canPaint = true
  }
  
  // **** Any, AnyRef and AnyVal are at the top of the Scala type hierarchy (fully qualified name is scala.Any etc)
  // **** AnyRef is the base class of every ref type. It is mapped to java.lang.Object
  val anyref: AnyRef = new Joker                  //> anyref  : AnyRef = lec_03_02_class_organization.lec_03_02_class_organizatio
                                                  //| n$Joker@3676481e
  // **** AnyVal is the base class of all value types
  val anyval: AnyVal = 2                          //> anyval  : AnyVal = 2
  
  // **** Any if the base class of AnyRef as well as AnyVal
  // **** Any defines ==, !=, equals, hashCode, toString
  var any: Any = anyref                           //> any  : Any = lec_03_02_class_organization.lec_03_02_class_organization$Joke
                                                  //| r@3676481e
  any = anyval
  
  // **** Nothing is at the bottom of the Scala type hierarchy
  // It is used 1. to signal abnormal termination, 2. as an element type of empty collection
  def gimmeNothing = throw new NoSuchElementException
                                                  //> gimmeNothing: => Nothing
  def error(msg: String) = throw new Error(msg)   //> error: (msg: String)Nothing
  
  // **** Null is the other type at the bottom of the Scala type hierarchy. It is the subclass of every other class.
  // So, in a way, it is the inverse of AnyRef.(It's not exactly its inverse because the inverse of AnyRef doesn't even make sense).
  // **** The constant null is of type Null, and hence can be assigned to any ref value.
  val x = null                                    //> x  : Null = null
  val s: String = null                            //> s  : String = null
  // val i: Int = null // ERROR because Int is a value type
 
  // **** type inference doesnt mind going up the type hierarchy until it finds a type that satisfies all branches.
  val t = if (true) 1 else false                  //> t  : AnyVal = 1
  // val t2: Int = if (true) 1 else false // ERROR type mismatch
}
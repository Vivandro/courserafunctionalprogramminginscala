package lec_03_01_class_hierarchies

object lec_03_01_class_hierarchies {
  println("Welcome to the Scala worksheet")       //> Welcome to the Scala worksheet
  
  // classes for sets of integers
  // abstract intset with functions -> incl adds an int, contains checks if the int exists
  // add toString in the concrete subclasses
  abstract class IntSet { // **** abstract classes
    def incl(x: Int): IntSet
    def betterIncl(x: Int): IntSet // this adds some efficiency to Martin's example of persistent BST
    def contains(x: Int): Boolean
    def union(that: IntSet): IntSet
    def foreach(f:Int => Unit): Unit
    def betterUnion(that: IntSet) = that // default works perfectly for Empty
  }
  
  // Empty extends IntSet
  object Empty extends IntSet { // **** singleton object
    // **** Forward reference require no special declarations if it's within the same file
    def incl(x: Int): IntSet = new NonEmpty(x, Empty, Empty) // // **** don't use new for singleton objects
    def betterIncl(x: Int) = incl(x)
    def contains(x: Int) = false
    def union(that: IntSet) = that
    def foreach(f:Int => Unit) = {} // Since we have no elements to work with, we don't.
    
    override def toString(): String = "E"
  }
  
  // NonEmpty extends IntSet
  // Stores the entries as a BST. No duplicates. So, if you try, to add dups, you should be unable to.
  // Should be a persistent data structure. So including a new node generates a new set.
  class NonEmpty(elem: Int, left: IntSet, right: IntSet) extends IntSet {// **** new subclass with a different constructor
    def incl(x: Int): IntSet =
      if (x < elem) new NonEmpty(elem, left incl x, right)      // left incl x
      else if (x > elem) new NonEmpty(elem, left, right incl x) // right incl x
      else this // this? or new? if we did reach this, then the other new NonEmpty's we created are pointless
                // We could potentially use contains to avoid this. Needs to be used in the caller once in order
                // to avoid adding exponential complexity that would result from adding it in incl.
                // Or....
    def betterIncl(x: Int): IntSet = { // Ofcourse, this means that to replace an item, you need to remove it from
                                       // the set and add the new value. I have no remove method right now.
      if (x < elem) {
        val newLeft = left betterIncl x
        if (newLeft == left) this   // **** ?? Not sure if this only compares references, but that's what we want.
        else new NonEmpty(elem, newLeft, right)
      } else if (x > elem) {
        val newRight = right betterIncl x
        if (newRight == right) this
        else new NonEmpty(elem, left, newRight)
      } else this
    }

    def contains(x: Int) =
      if (x < elem) left contains x
      else if (x > elem) right contains x
      else true
      
    def union(that: IntSet) = ((left union right) union that) incl elem
    def foreach(f: Int => Unit): Unit = {
      // Performing the f in inorder fashion. Chose inorder at random. Use something else, or make it configurable
      // if that makes sense in the context of it's use.
      left foreach f
      f(elem)
      right foreach f
    }
    
    // **** need the override keyword here because the superclass has an implementation of this method.
    // It was optional for other methods since it didn't have an implementation for others.
    override def betterUnion(that: IntSet): IntSet = {
      // Since we have no way of knowing how many elements each set has, we will just iterate over that.
      // It would be useful to add the count to the sets in order to improve the average running times.
      var rv = Empty union this // union is so flaky that "this union Empty" would be O(n) while "Empty union this" is O(1)
      that.foreach(x => rv = rv.incl(x))
      
      rv
    }
    
    // Comment toString out in case we want to see the references printed out.
    // Need that to see if betterIncl actually returns the same set in case x already exists in the set.
    override def toString = "( " + left + ", " + elem + ", " + right + ")"
  }
  
  /* **** Terminology
     Empty extends class IntSet
     Empty conforms to type IntSet, so it can be used wherever IntSet is required.
     IntSet is superclass
     Empty is it's subclass
     
     IntSet an Object are base classes of Empty
   */
  
  val set1 = new NonEmpty(50, Empty, Empty)       //> set1  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = 
                                                  //| ( E, 50, E)
  val set2 = Empty incl 75                        //> set2  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| E, 75, E)
  val set3 = set1 incl 30                         //> set3  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, E)
  val set4 = set3 incl 100                        //> set4  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, ( E, 100, E))
  val set5 = set3 incl 60                         //> set5  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, ( E, 60, E))
  val set6 = set4 incl 60                         //> set6  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, ( ( E, 60, E), 100, E))
  val set7 = set6 betterIncl 60                   //> set7  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, ( ( E, 60, E), 100, E))
  val set8 = set7 betterIncl 60                   //> set8  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( 
                                                  //| ( E, 30, E), 50, ( ( E, 60, E), 100, E))
  val set9 = new NonEmpty(3, Empty, Empty)        //> set9  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = 
                                                  //| ( E, 3, E)
  set8 union set9                                 //> res0: lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( E,
                                                  //|  3, ( ( E, 30, ( E, 50, E)), 60, ( E, 100, E)))
                                                  
  /* dry run
  
           100                             25
    50           75                   E       E
E      E    E      E



(.100. u .25.)

=> ( (.50. u .75.) u .25. ) incl 100
=> ( ( (E u E) u .75.) incl 50) u .25. ) incl 100
=> E u .75.) incl 50) u .25.) incl 100
=> .75. incl 50) u .25.) incl 100
=> .75. u .25.) incl 100
=> 50 u E) u 25) incl 75) incl 100
=> E u E ) u E) incl 50) u .25.) incl 75) incl 100
=> E u E) incl 50) u .25.) incl 75) incl 100
=> E incl 50) u .25.) incl 75) incl 100
=> .50. u .25.) incl 75) incl 100
=>E u E) u .25.) incl 50) incl 75) incl 100
=> E u .25.) incl 50) incl 75) incl 100
=> .25. incl 50 incl 75 incl 100

          25
        E    50
            E   75
               E  100
                 E    E

  So, this may not be the shortest route to getting the union, but it does terminate because it relies on the following:
  1. Empty union X will reduce the expresion to X
  2. X incl a will reduce the expression to X
  3. Every Y will eventually be whittled down to E incl component of Y incl another component of Y... in ascending order(since it's a BST)
  Note: Interestingly, we've written our toString implementation to perform an inorder traversal of the BST.
  So, overall, we break the whole BST down to the ground and convert it to E incl a incl b incl c ... incl z
  This is equivalent to breaking a BST down and rebuilding it as a sorted linked list. Which might be useful
  in some cases. Like making all student playing on the ground suddenly assemble together and march
  in a line while standing in increasing order of their heights. We could as well use inorder traversal alone
  for this. Hmm...would be useful if the BST is to be used in an online algorithm at the point when the
  online-ness of the algorithm is taken away. Like moving entries out of a database and into a backup file.
  The backup files would generally be costly in terms of random access. But linear access is cheap. Also, during
  recovery, we could replay those entries with a strong assumption of them coming in in a sorted order.
  
  A better version of union would actually go through the smaller of the two sets and add all it's elements to the larger set.
  I'll need a foreach for that.
  
  */
  val v50 = new NonEmpty(50, Empty, Empty)        //> v50  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = (
                                                  //|  E, 50, E)
  val v75 = new NonEmpty(75, Empty, Empty)        //> v75  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = (
                                                  //|  E, 75, E)
  val v100 = new NonEmpty(100, v50, v75)          //> v100  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = 
                                                  //| ( ( E, 50, E), 100, ( E, 75, E))
  val v25 = new NonEmpty(25, Empty, Empty)        //> v25  : lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.NonEmpty = (
                                                  //|  E, 25, E)
  v100 union v25                                  //> res1: lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( E,
                                                  //|  25, ( E, 50, ( E, 75, ( E, 100, E))))
  // Confirmed - The result is exactly what I came up with in the dry run.
  
  v100 betterUnion v25                            //> res2: lec_03_01_class_hierarchies.lec_03_01_class_hierarchies.IntSet = ( ( 
                                                  //| ( E, 25, E), 50, E), 100, ( E, 75, E))
 // Confirmed
  
}